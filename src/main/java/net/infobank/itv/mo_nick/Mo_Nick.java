package net.infobank.itv.mo_nick;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.infobank.itv.mo_common.model.ConsumerInfo;
import net.infobank.itv.mo_common.util.FileConfig;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_nick.kafka.ConsumerThread;
import net.infobank.itv.mo_nick.kafka.ConsumerThread2;
import net.infobank.itv.mo_nick.util.ScheduleUtils;

@SpringBootApplication
public class Mo_Nick {
    private static Logger log = LoggerFactory.getLogger(Mo_Nick.class);

    public static FileConfig _config;
    public HashMap<String, Object> thread_list;
    public ConsumerInfo conInfo;

    public static void main(String[] args) {
        Mo_Nick pgmAssign = new Mo_Nick();
        pgmAssign.start(args);
    }

    public void start(String[] args) {
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
        
        // 접속정보 를 파일에서 가져온다.
        if (args.length != 1) {
            System.err.println("error: not enough argument");
            System.err.println("<arg1:config file>");
            return;
        }

        _config = new FileConfig(args[0]);

        String group_name = new Object() {}.getClass().getEnclosingClass().getSimpleName();
        MDC.put("log_file", _config.getLog_dir() + "/" + group_name+ "/" + group_name);
        
        Properties props = new Properties();
        props.put("driver", _config.getDriver());
        props.put("url", _config.getUrl());
        props.put("user", _config.getUser());
        props.put("password", _config.getPw());
        MyBatisConnectionFactory.setSqlSessionFactory(props);

        String mo_schedule_term = _config.getMoScheduleTerm(); // 스케쥴 확인 (초)
        String kafka_consumer_cnt = _config.getKafkaConsumerCnt(); // consumer 갯수
        int consumer_count = Integer.parseInt(kafka_consumer_cnt);
        
        String version = _config.getVersion(); // 버전 
        int nVersion = Integer.parseInt(version);

        conInfo = new ConsumerInfo();
        conInfo.setBoot_strap_server(_config.getKafkaBroker());
        conInfo.setPre_group_name(group_name);
        conInfo.setReload_sec(Integer.parseInt(mo_schedule_term));

        thread_list = new HashMap<String, Object>();

        ArrayList<String> new_arr = ScheduleUtils.getMOList();
        for (int i = 0; i < consumer_count; i++) {                    
        	if (nVersion == 2) {
        		insertConsumerThread2(new_arr, i);
        	}
        	else {
        		insertConsumerThread(new_arr, i);
        	}
        }
    }

    public void insertConsumerThread(ArrayList<String> mo, int i) {
        conInfo.setMo_num(mo);

        Runnable consumer = new ConsumerThread(conInfo);
        Thread thread_consumer = new Thread(consumer);
        thread_consumer.start();

        log.info("id : " + thread_consumer.getId() + " name : " + thread_consumer.getName());
        thread_list.put(i + "", consumer);
    }
    


    public void insertConsumerThread2(ArrayList<String> mo, int i) {
        conInfo.setMo_num(mo);

        Runnable consumer = new ConsumerThread2(conInfo);
        Thread thread_consumer = new Thread(consumer);
        thread_consumer.start();

        log.info("~~~ VERSION 2 ");
        log.info("id : " + thread_consumer.getId() + " name : " + thread_consumer.getName());
        thread_list.put(i + "", consumer);
    }

    
    public class ShutdownThread extends Thread {
        public void run() {
            for (int i = 0; i < thread_list.size() ; i++) {
                ConsumerThread thread = (ConsumerThread) thread_list.get(i+"");
                thread.shutdown();
            }
        }
    }
}
