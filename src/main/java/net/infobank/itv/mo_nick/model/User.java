package net.infobank.itv.mo_nick.model;

import lombok.Data;

@Data
public class User {

	private int pgm_key;
	private String user_id;
	private String user_name;
	private String msg_com;
	private String user_reg_dt;
	private String prelogin_dt;
	private String lastlogin_dt;
	
	private String user_nick;
}
