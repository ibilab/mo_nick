package net.infobank.itv.mo_nick.kafka;

import java.io.StringReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import net.infobank.itv.mo_common.model.ConsumerInfo;
import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.util.CommonUtils;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_nick.Mo_Nick;
import net.infobank.itv.mo_nick.dao.UserDAO;
import net.infobank.itv.mo_nick.model.User;
import net.infobank.itv.mo_nick.util.ScheduleUtils;

public class ConsumerThread2 implements Runnable {
    private static Logger log = LoggerFactory.getLogger(ConsumerThread2.class);

    String BootStrapServer;
    ArrayList<String> Monum;
    String Pre_Groupname;
    int RefreshTimer;
    int ReLoad_Sec;
    
    // Sub Consumer
    KafkaConsumer<String, String> consumer = null;
    private Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();

    private final AtomicBoolean running = new AtomicBoolean(false);

    public ConsumerThread2(ConsumerInfo Info) {
        // store parameter for later user
        BootStrapServer = Info.getBoot_strap_server();
        Monum = Info.getMo_num();
        Pre_Groupname = Info.getPre_group_name();
        ReLoad_Sec = Info.getReload_sec();
    }

    public void shutdown() {
        log.info("Stop this thread");
        running.set(false);
    }

    private class HandleRebalance implements ConsumerRebalanceListener {
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) { 
        }

    	public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
    	    log.info("Lost partitions in rebalance. " +
    	            "Committing current offsets:" + currentOffsets);
    	    consumer.commitSync(currentOffsets); 
    	    
    	    // Reblanacing error add 
    	    currentOffsets.clear(); // 추가 코드
	    }
    }
    
	@Override
    public void run() {
        Thread shutDownHook = new ShutdownHook(Thread.currentThread(), "Shutdown");
        Runtime.getRuntime().addShutdownHook(shutDownHook);
        
        log.info("run thread : " + Thread.currentThread().getName());

        String group;
        String message = null;
        Gson g = new Gson();
        int GapTime = 0;
        int CurrentTime = 0;

        // init Reload time
        CurrentTime = (int) (System.currentTimeMillis() / 1000);
        GapTime = CurrentTime;

        // 쓰레드 스탑
        running.set(true);

        // Get Schedule
//        List<Schedule> list = ScheduleUtils.getSchedule();
        
        // sUB sETTING
        group = Pre_Groupname;
        log.info("groupname : " + group);

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BootStrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<String, String>(props);

        try {
            consumer.subscribe(Monum, new HandleRebalance());
            //insertTopic(Monum);

        	while (running.get()) {
          	
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, String> record : records) {
                    try {
                        message = record.value();
                        
                        log.info("topic = {}, partition = {}, offset = {}, key = {}, msg = {}",
                                record.topic(), record.partition(), record.offset(),
                                record.key(), record.value());
                        
                        // Msg JSon 변환
                        JsonReader reader_body = new JsonReader(new StringReader(message));
                        reader_body.setLenient(true);
                        ProgramMsg body = g.fromJson(reader_body, ProgramMsg.class);
                        
                        // 1. 유저 테이블 insert (update & insert) 
                        // 2, Mo Count 처리
                        User user = new User();
                        initUserModel(user, body);
                        
                        UserDAO userDAO = new UserDAO(MyBatisConnectionFactory.getSqlSessionFactory());
                        userDAO.insertUpdate_2(user);
                        
                        String radio_flag = Mo_Nick._config.getRadioFlag();
                        
                        if("1".equals(radio_flag)) {
                            String nick = CommonUtils.findNick(body.getMsg_data());
                            if(nick.length() > 0) {
                                if(nick.getBytes().length > 32) nick = nick.substring(0, 32);
                                user.setUser_nick(nick);
                                userDAO.updateNickname_2(user);
                            }
                        }
                        
                        // 수동으로 commit 및 seek 처리
                        currentOffsets.put(
                                new TopicPartition(record.topic(), record.partition()),
                                new OffsetAndMetadata(record.offset()+1, null));
                    } catch(Exception e) {
                        log.error("Exception : ", e);
                    }
                }
                
                consumer.commitAsync(currentOffsets, null);

                // Schedule ReLoad
                CurrentTime = (int) (System.currentTimeMillis() / 1000);
                if (CurrentTime - GapTime >= ReLoad_Sec) {
                    GapTime = CurrentTime;
                    
                    ArrayList<String> mo_list = ScheduleUtils.getMOList();
                    if(!Arrays.equals(Monum.toArray(), mo_list.toArray())) {
                    	log.info(" Topic Change & Close & Re-Sub  ");
                    	consumer.subscribe(mo_list, new HandleRebalance());
                        // insertTopic(mo_list);
                    	Monum = mo_list;
                    }
                    
                }

        	} 
        } catch (WakeupException e) {
            // ignore, we're closing
        } catch(Exception e) {
            log.error(message, e);
        } finally {
            try {
                consumer.commitSync(currentOffsets);
            } finally {
                consumer.close();
    	        log.info("Exit thread : " + Thread.currentThread().getName());
            }
        }
    }

    private void initUserModel (User user, ProgramMsg body) {
        user.setPgm_key(body.getPgm_key());
        user.setUser_id(body.getMsg_username());
        user.setUser_name(body.getMsg_username());
        user.setMsg_com(body.getMsg_com());
        user.setUser_reg_dt(body.getMsg_date());
        user.setPrelogin_dt(body.getMsg_date());
        user.setLastlogin_dt(body.getMsg_date());
    }

    private class ShutdownHook extends Thread {
        private Thread target;
        
        public ShutdownHook(Thread target, String name) {
            super(name);
            this.target = target;
        }
        
        public void run() {
            shutdown();
            
            try {
                //target 쓰레드가 종료될 때 까지 기다린다.
                target.join();                                
            } catch (InterruptedException e) {
                log.error("sth wrong in shutdown process", e);
            }
        }
    }
}
