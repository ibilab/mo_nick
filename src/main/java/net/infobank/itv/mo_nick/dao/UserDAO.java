package net.infobank.itv.mo_nick.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_nick.model.User;

public class UserDAO {
    private static Logger log = LoggerFactory.getLogger(UserDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public UserDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }


    public void insertUpdate(User user){

        SqlSession session = sqlSessionFactory.openSession();

        try {
            session.insert("User.insertUpdate", user);
            session.commit();
        } catch(Exception e)  {
            log.error("User.insertUpdate : " , e);
            session.rollback();
        } finally {
            session.close();
        }
    }
    
    public void insertUpdate_2(User user){

        SqlSession session = sqlSessionFactory.openSession();

        try {
            session.insert("User.insertUpdate_2", user);
            session.commit();
        } catch(Exception e)  {
            log.error("User.insertUpdate_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }
    }
    
    
    public void updateNickname(User user){
        
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            session.insert("User.updateNickname", user);
            session.commit();
        } catch(Exception e)  {
            log.error("User.updateNickname : " , e);
            session.rollback();
        } finally {
            session.close();
        }
    }
    
    public void updateNickname_2(User user){
        
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            session.insert("User.updateNickname_2", user);
            session.commit();
        } catch(Exception e)  {
            log.error("User.updateNickname_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }
    }

}
